json.array!(@bookings) do |booking|
  json.extract! booking, :id, :band_id, :club_id, :fee, :date
  json.url booking_url(booking, format: :json)
end
