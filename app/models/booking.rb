class Booking < ActiveRecord::Base
  belongs_to :band
  belongs_to :club

  validates_presence_of :fee, :date , :band_id, :club_id
  validates :fee, :numericality => { :greater_than_or_equal_to => 0 }
end
