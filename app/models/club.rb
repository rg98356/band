class Club < ActiveRecord::Base
  has_many :bookings
  has_many :bands, through: :bookings
  validates_presence_of :name ,:street_address
end
