class Band < ActiveRecord::Base
  has_many :bookings
  has_many :clubs, through: :bookings
  validates_presence_of :name ,:num_members
end
