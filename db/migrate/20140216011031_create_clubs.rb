class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name
      t.string :street_address
      t.integer :club_id

      t.timestamps
    end
  end
end
