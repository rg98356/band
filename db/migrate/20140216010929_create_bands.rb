class CreateBands < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :name
      t.integer :num_members
      t.integer :band_id

      t.timestamps
    end
  end
end
